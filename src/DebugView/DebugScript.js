
export function resetDebugClass(className) {
    let elements = document.getElementsByClassName(className);
    for (let i = 0; i < elements.length; i++) {
        elements[i].classList.remove("renderCheck");
        void elements[i].offsetWidth;
        elements[i].classList.add("renderCheck");
    }
}

export function getRandomNumber() {
    return Math.floor(Math.random() * 1000000000);
}
