import { useEffect, useRef } from "react";
import './DebugStyle.css'
import { resetDebugClass, getRandomNumber } from './DebugScript';

export default function DebugBlock(props) {

    const rndID = useRef(getRandomNumber());

    function getDefaultClass() {
        return "debugblock debugId_" + rndID.current;
    }

    useEffect(() => {
        resetDebugClass("debugId_" + rndID.current);
    });

    return (
        <>
            <div className={getDefaultClass()}>{props.children}</div>
        </>
    );
}
