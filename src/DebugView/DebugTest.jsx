import { useState } from "react";
import DebugBlock from "./DebugBlock";

export default function DebugTest() {
    const [count, setCount] = useState(0);
    return (
        <>
            <DebugBlock dcount={count}>
                <div>Count = {count}</div>
                <button onClick={() => setCount(count + 1)}>Add</button>
                <button onClick={() => setCount(count - 1)}>Sub</button>
            </DebugBlock>
        </>
    );
}
