import { useState } from "react";

function CounterFunction() {
    const [count, setCount] = useState(0);
    return (
    <>
        <h2>Function</h2>
        <CounterDisplay count={count} />
        <button onClick={() => setCount(count + 1)}>Add</button>
    </>
    );
}

function CounterDisplay(props) {
    return(
        <h4>Current count : {props.count}</h4>
    );
}

export default CounterFunction;
