import CounterClass from "./CounterClass";
import CounterFunction from "./CounterFunction";

export default function CounterDisplay() {
    return (
    <>
        <CounterClass />
        <br />
        <CounterFunction />
    </>
    );
}
