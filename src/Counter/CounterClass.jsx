import React from "react";

class CounterClass extends React.Component {
    constructor() {
        super();
        this.state = { count: 0 };
    }
    render() {
        return <>
            <h2>Class</h2>
            <CounterDisplay count={this.state.count} />
            <button onClick={() => this.setState({ count: this.state.count + 1 })}>Add</button>
        </>;
    }
}

class CounterDisplay extends React.Component {
    render() {
        return <h4>Current count : {this.props.count}</h4>;
    }
}

export default CounterClass;
