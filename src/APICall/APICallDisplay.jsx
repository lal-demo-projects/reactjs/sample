import { useState, useEffect } from "react";

export default function APICallDisplay() {
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => { setData(data); })
            .catch(error => setError(error));
    }, []);

    return <>
        { data && <DataDisplay dataItems={data} /> }
        { error && <div>{error}</div> }
        { (data == null && error == null) && <div>Loading data...</div> }
    </>;
}

function DataDisplay(props) {
    return <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>User name</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            {
                props.dataItems.map((item, i) => {
                    return (
                        <tr key={i}>
                            <td>{item.id}</td>
                            <td>{item.name}</td>
                            <td>{item.username}</td>
                            <td>{item.email}</td>
                        </tr>
                    )
                })
            }
        </tbody>
    </table>
}
