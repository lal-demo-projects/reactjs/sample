import { React, memo, useState } from "react";
import DebugBlock from "../DebugView/DebugBlock";
import useToggle from "../CustomHook/useToggle";
import useTime from "../CustomHook/useTime";

export default function MemoDisplay() {
    const [count, setCount] = useState(0);
    const [toggle, setToggle] = useToggle(false);
    const timeVal = useTime();
    return (
        <>
            <div>Memo</div>
            <br />
            <div>Count {count}</div>
            <br />
            <div>Time {timeVal.getHours() + " " + timeVal.getMinutes() + " " + timeVal.getSeconds()}</div>
            <br />
            <button onClick={() => setCount(count + 1)}>Add</button>
            <button onClick={() => setCount(count - 1)}>Sub</button>
            <button onClick={() => setToggle()}>Toggle</button>
            <br />
            <NormalBlock name="Normal Block" flag={toggle} count={count} />
            <br />
            <ConvertPureMemoBlock name="Convert Normal to Memo" flag={toggle} count={count} />
            <br />
            <PureMemoBlock name="Memo Block" flag={toggle} count={count} />
            <br />
            <PureMemoPropBlock name="Props Memo Block for count 5" flag={toggle} count={count} />
        </>
    );
}

function BlockBase(props) {
    return (
        <>
            <div>{props.name}</div>
            <div>Toggle {props.flag ? "true" : "false" }</div>
            <div>Count {props.count}</div>
        </>
    );
}

function NormalBlock(props) {
    return (
        <DebugBlock>
            <BlockBase name={props.name} flag={props.flag} count={props.count} />
        </DebugBlock>
    );
}

const ConvertPureMemoBlock = memo(NormalBlock);

// Memo won't work with props unless it's checked
// With parameter value
const PureMemoBlock = memo(({ name, flag, count }) => {
    return (
        <DebugBlock>
            <BlockBase name={name} flag={flag} count={count} />
        </DebugBlock>
    );
});

// Memo with props check
// With custom check for render
// Check return true if the props are same (It will skip render if result is true)
const PureMemoPropBlock = memo((props) => {
    return (
        <DebugBlock>
            <BlockBase name={props.name} flag={props.flag} count={props.count} />
        </DebugBlock>
    );
}, (prevProps, nextProps) => (nextProps.count !== 5));
