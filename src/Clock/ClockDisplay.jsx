import ClockClass from "./ClockClass";
import ClockFunction from "./ClockFunction";

export default function ClockDisplay() {
    return <>
        <ClockClass />
        <br />
        <ClockFunction />
    </>;
}
