import React from "react";

class ClockClass extends React.Component {
    constructor() {
        super();
        this.state = { time: new Date() };
    }

    componentDidMount() {
        this.timerID = setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            time: new Date(),
        });
    }

    render() {
        return (
            <>
                <h2>Class</h2>
                <h4>Current time is {this.state.time.toLocaleTimeString()}.</h4>
            </>
        );
    }
}

export default ClockClass;
