import { useState, useEffect } from "react";

function ClockFunction() {
    const [time, setTime] = useState(new Date());

    useEffect(() => {
        let timer = setTimeout(() => {
            setTime(new Date());
        }, 1000);

        return () => clearTimeout(timer);
    });

    return <>
        <h2>Function</h2>
        <h4>Current time is {time.toLocaleTimeString()}.</h4>
    </>;
}

export default ClockFunction;
