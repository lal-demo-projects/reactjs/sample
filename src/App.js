import logo from "./logo.svg";
import "./App.css";
import { Link } from "react-router-dom";

function App() {
    return (
        <div className="App">
            <img src={logo} className="App-logo" alt="logo" />
            <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/CounterDisplay">Counter</Link></li>
                <li><Link to="/ClockDisplay">Clock</Link></li>
                <li><Link to="/DebugTest">Debug</Link></li>
                <li><Link to="/APICallDisplay">API Call</Link></li>
                <li><Link to="/MemoDisplay">Memo</Link></li>
            </ul>
        </div>
    );
}

export default App;
